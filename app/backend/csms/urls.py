from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from ninja import NinjaAPI

from students.api import router as students_router


api = NinjaAPI(title="CSMS API", docs_url="/")
api.add_router("/students", students_router, tags=["students"])


urlpatterns = (
    [
        path("admin/", admin.site.urls),
        path("api/v1/", api.urls),
    ]
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
)
