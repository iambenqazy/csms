from ninja import ModelSchema, Schema

from students.models import Student


class NotFoundSchema(Schema):
    message: str


class StudentIn(ModelSchema):
    class Config:
        model = Student
        model_fields = ["last_name", "first_name", "other_names", "date_of_birth"]


class StudentOut(ModelSchema):
    class Config:
        model = Student
        model_fields = ["id", "last_name", "first_name", "other_names", "student_id", "date_of_birth"]