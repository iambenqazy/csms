import uuid

from django.db import models

from students.utils import unique_student_id_generator, upload_to_path_rename


class CustomManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_deleted=False)


class BaseSetup(models.Model):
    is_deleted = models.BooleanField(editable=False, default=False)
    updated_on = models.DateTimeField(editable=False, auto_now=True)
    created_on = models.DateTimeField(editable=False, auto_now_add=True)
    id = models.UUIDField(
        primary_key=True, unique=True, default=uuid.uuid4, editable=False
    )

    class Meta:
        abstract = True


class Student(BaseSetup):
    GENDER = [
        ("Male", "Male"),
        ("Female", "Female"),
    ]

    first_name = models.CharField(max_length=50)
    other_names = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=50)
    profile_image = models.ImageField(
        upload_to=upload_to_path_rename, default="default_dp.png", null=True, blank=True
    )
    email = models.EmailField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=10, choices=GENDER, null=True, blank=True)
    program = models.ForeignKey(
        "Program",
        related_name="program",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    house = models.ForeignKey(
        "House", related_name="house", on_delete=models.SET_NULL, null=True, blank=True
    )
    subjects = models.ManyToManyField("Subject", null=True, blank=True)
    student_id = models.CharField(max_length=50, unique=True, null=True, blank=True)

    class Meta:
        ordering = ["last_name"]

    def __str__(self):
        return f"{self.last_name}, {self.first_name} {self.other_names}"

    # return the url of a student instance
    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("students:student_detail", kwargs={"pk": self.pk})

    @property
    def full_name(self) -> str:
        """return the fullname of the member"""
        if self.other_names:
            return f"{self.last_name.upper()}, {self.first_name.title()} {self.other_names.title()}"
        else:
            return f"{self.last_name.upper()}, {self.first_name.title()}"

    def save(self, *args, **kwargs) -> None:
        """
        Change letter casing when saving to database and
        insert the unique member id to the student_id field when saving
        """

        self.last_name = self.last_name.upper()
        self.first_name = self.first_name.title()

        if self.other_names:
            self.other_names = self.other_names.title()

        # Check if the object has a primary key (i.e., has been saved to the database)
        if not self.created_on:
            # Query the database for existing student numbers and generate a unique one
            student_id_list = list(
                Student.objects.only("student_id")
                .values_list("student_id", flat=True)
                .order_by("student_id")
            )  # return the list of student_id's
            self.student_id = unique_student_id_generator(
                student_id_list, "A"
            )  # generate the unique member id

        # Call the superclass save method to save the object to the database
        return super().save(*args, **kwargs)


class House(BaseSetup):
    name = models.CharField(max_length=50, unique=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]

    def save(self, *args, **kwargs) -> None:
        self.name = self.name.upper()

        # Call the superclass save method to save the object to the database
        return super().save(*args, **kwargs)


class Program(BaseSetup):
    name = models.CharField(max_length=100, unique=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]

    def save(self, *args, **kwargs) -> None:
        self.name = self.name.upper()

        # Call the superclass save method to save the object to the database
        return super().save(*args, **kwargs)


class Subject(BaseSetup):
    name = models.CharField(max_length=100)
    subject_code = models.CharField(max_length=50, unique=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.subject_code

    class Meta:
        ordering = ["name"]

    def save(self, *args, **kwargs) -> None:
        self.name = self.name.upper()
        self.subject_code = self.subject_code.upper()

        # Call the superclass save method to save the object to the database
        return super().save(*args, **kwargs)
