from django.shortcuts import get_object_or_404
from django.db.models import Q

from ninja import Router
from ninja.pagination import paginate

from students.schema import NotFoundSchema, StudentOut, StudentIn

from students.models import Student


router = Router()


@router.post("/", response={200: StudentOut})
def add_student(request, payload: StudentIn):
    return Student.objects.create(**payload.dict())


@router.get("/", response=list[StudentOut])
@paginate
def students_list(request):
    return Student.objects.exclude(is_deleted=True)


@router.get("/{str:student_id}/", response={200: StudentOut, 404: NotFoundSchema})
def student_details(request, student_id: str):
    return get_object_or_404(
        Student.objects.filter(Q(pk=student_id) & Q(is_deleted=False))
    )


@router.put("/{str:student_id}/", response={200: StudentOut, 404: NotFoundSchema})
def update_student(request, student_id: str, payload: StudentIn):
    student = get_object_or_404(
        Student.objects.filter(Q(pk=student_id) & Q(is_deleted=False))
    )

    for attribute, value in payload.dict().items():
        setattr(student, attribute, value)
    student.save()

    return student


@router.delete("/{str:student_id}/", response={404: dict})
def delete_student(request, student_id: str):
    student = get_object_or_404(
        Student.objects.filter(Q(pk=student_id) & Q(is_deleted=False))
    )
    student.is_deleted = True
    student.save()

    return 404, {}
