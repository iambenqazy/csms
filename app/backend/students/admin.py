from django.contrib import admin
from .models import Student, Subject, Program, House


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['student_id', 'last_name', 'first_name', 'other_names', ]
    list_filter = ['last_name']
    
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        # Modify the queryset here
        qs = qs.exclude(is_deleted=True)
        return qs


@admin.register(House)
class HouseAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    

admin.site.register(Subject)
admin.site.register(Program)