import os
import uuid
import re


def unique_student_id_generator(
    student_id_list: list[str], student_year_group_id: str
) -> str:
    """Generate a unique student id number from the last recorded number"""
    from string import ascii_letters

    # filter out non-string elements from the list
    student_id_list = [x for x in student_id_list if isinstance(x, str)]

    # extract numerical IDs from the list and increment by 1
    # numerical_ids = [int(x.lstrip(ascii_letters)) for x in student_id_list]
    numerical_ids = [int(re.findall(r"\d+", x)[0]) for x in student_id_list]

    student_id_number = max(numerical_ids, default=0) + 1

    # generate new student IDs until a unique one is found
    while True:
        student_id = f"{student_year_group_id.upper()}{student_id_number:03d}"
        if student_id not in student_id_list:
            return str(student_id)
        student_id_number += 1

    # This line should not be reached, but is included as a failsafe in case the loop runs indefinitely.
    raise Exception("No available student id number")


def upload_to_path_rename(instance, filename: str) -> str:
    """
    Rename image instance primary key on upload
    """
    ext = filename.split(".")[-1].lower()
    try:
        filename = f"{instance.pk}.{ext}"
    except AttributeError:
        filename = f"{uuid.uuid4().hex}.{ext}"

    # return the whole path to the file
    return os.path.join("student", "profile-images", filename)
